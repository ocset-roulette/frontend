import { Color, Field, ValueField } from '../types/roulette'

export const prepareFields = (fields: Field[]) => {
  const numbers: ValueField[] = []
  const zeros: ValueField[] = []

  fields.forEach(field => {
    if (field.color === Color.Green) {
      zeros.push({
        color: field.color,
        value: field.number,
        checked: false,
        chips: 0,
        multiple: 35,
      })
      return
    }
    numbers.push({
      color: field.color,
      value: field.number,
      checked: false,
      chips: 0,
      multiple: 35,
    })
  })

  const colors = [
    {
      color: Color.Black,
      value: Color.Black,
      checked: false,
      chips: 0,
      multiple: 1,
    },
    {
      color: Color.Red,
      value: Color.Red,
      checked: false,
      chips: 0,
      multiple: 1,
    },
  ]

  const columns = [
    {
      value: [1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34],
      label: '2to1',
      checked: false,
      chips: 0,
      multiple: 2,
    },
    {
      value: [2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35],
      label: '2to1',
      checked: false,
      chips: 0,
      multiple: 2,
    },
    {
      value: [3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36],
      label: '2to1',
      checked: false,
      chips: 0,
      multiple: 2,
    },
  ]

  const evenOdds = [
    {
      value: [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36],
      label: 'even',
      chips: 0,
      multiple: 1,
      checked: false,
    },
    {
      value: [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35],
      label: 'odd',
      chips: 0,
      multiple: 1,
      checked: false,
    },
  ]

  const dozens = [
    {
      value: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
      label: '1st 12',
      checked: false,
      chips: 0,
      multiple: 2,
    },
    {
      value: [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
      label: '2nd 12',
      checked: false,
      chips: 0,
      multiple: 2,
    },
    {
      value: [25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36],
      label: '3rd 12',
      checked: false,
      chips: 0,
      multiple: 2,
    },
  ]

  const smallBig = [
    {
      value: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18],
      label: '1 to 18',
      chips: 0,
      multiple: 1,
      checked: false,
    },
    {
      value: [19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36],
      label: '19 to 36',
      chips: 0,
      multiple: 1,
      checked: false,
    },
  ]

  return {
    numbers: numbers.sort((a, b) => {
      if (Number(a.value) < Number(b.value)) return -1
      if (Number(a.value) > Number(b.value)) return 1
      return 0
    }),
    columns: columns,
    evenOdds: evenOdds,
    dozens: dozens,
    smallBig: smallBig,
    colors: colors,
    zeros: zeros,
  }
}

export const generateRandomRouletteResult = (numbers: any[]) => {
  return numbers[Math.floor(Math.random() * numbers.length)]
}
