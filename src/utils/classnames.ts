/* simple class utility - replacement of classnames package */

export const classnames = (...args: any[]) => {
  const classes: string[] = []

  args.forEach(arg => {
    if (typeof arg === 'string') {
      return classes.push(arg)
    } else if (typeof arg === 'object') {
      for (const key in arg) {
        if (arg[key]) {
          classes.push(key)
        }
      }
    }
  })

  return classes.join(' ')
}

export default classnames
