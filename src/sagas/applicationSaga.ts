import { AnyAction } from 'redux'
import { takeEvery, put } from 'redux-saga/effects'

import {
  createAddHistoryMessageAction,
  createGetResultsAction,
  createSaveInitialChipsAction,
  createSaveUserNameAction,
  createSelectRouletteVariantAction,
  createSetGameStatusAction,
} from '../actions'
import {
  CALL_API,
  END_GAME,
  GET_RESULTS,
  POST_RECORD_SUCCESS,
  SEND_RECORD,
  SHOW_RESULTS,
  START_GAME,
} from '../constants'
import { GameStatus, Request } from '../types'

const api = 'https://localhost:44322/api/Results'

export function* applicationSaga() {
  yield takeEvery(START_GAME, startGameSaga)
  yield takeEvery(END_GAME, endGameSaga)
  yield takeEvery(SEND_RECORD, sendRecordSaga)
  yield takeEvery(SHOW_RESULTS, showResults)
  yield takeEvery(GET_RESULTS, getResultsSaga)
  yield takeEvery(CALL_API, callApi)
  yield takeEvery(POST_RECORD_SUCCESS, showResults)
}

export const startGameSaga = function* (action: AnyAction) {
  yield put(createSaveUserNameAction(action.payload.userName))
  yield put(createSaveInitialChipsAction(action.payload.initialChips))

  yield put(
    createAddHistoryMessageAction({
      msg: 'initialAdd',
      chips: action.payload.initialChips,
    }),
  )

  yield put(createSelectRouletteVariantAction(action.payload.rouletteVariant))
  yield put(createSetGameStatusAction(GameStatus.running))
}

const callApi = function* (action: AnyAction) {
  const request: Request = { method: action.payload.request.method }

  if (request.method !== 'GET') {
    request.headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    }
    request.body = action.payload.request.body
  }

  const [requestType, successType, errorType] = action.payload.types

  yield put({ type: requestType })

  try {
    const response: Response = yield fetch(api, request)
    const data: JSON = yield response.json()
    yield put({ type: successType, payload: { response: data } })
  } catch (error) {
    yield put({ type: errorType, payload: { error: error.message } })
  }
}

const endGameSaga = function* () {
  yield put(createSetGameStatusAction(GameStatus.end))
}

const sendRecordSaga = function* (action: AnyAction) {
  yield callApi(action)
}

const getResultsSaga = function* (action: AnyAction) {
  yield callApi(action)
}

const showResults = function* () {
  yield put(createSetGameStatusAction(GameStatus.results))
  yield put(createGetResultsAction())
}
