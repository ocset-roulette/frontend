import { all, fork } from 'redux-saga/effects'
import { applicationSaga } from './applicationSaga'
import { rouletteSaga } from './rouletteSaga'

export function* rootSaga() {
  yield all([fork(applicationSaga)])
  yield all([fork(rouletteSaga)])
}
