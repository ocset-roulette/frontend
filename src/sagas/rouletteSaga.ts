import { AnyAction } from 'redux'
import { put, select, takeEvery } from 'redux-saga/effects'

import {
  createAddHistoryMessageAction,
  createCleanDeskAction,
  createIncreaseBetChipsAction,
  createIncreaseGameAction,
  createIncreaseLostChipsAction,
  createIncreaseWonChipsAction,
  createLastWonNumberAction,
  createLostGameAction,
  createPlaceChipsAction,
} from '../actions'
import { SET_BET, SET_ROULETTE_START } from '../constants'
import { getRouletteFields, getSelectedField, getUserChips } from '../selectors'
import { BasicField, FieldLocation, Fields, GroupField, ValueField } from '../types/roulette'
import { generateRandomRouletteResult } from '../utils/roulette'

export function* rouletteSaga() {
  yield takeEvery(SET_BET, betSaga)
  yield takeEvery(SET_ROULETTE_START, rouletteStartSaga)
}

export const betSaga = function* (action: AnyAction) {
  const selectedField: FieldLocation = yield select(getSelectedField)
  const fields: Fields = yield select(getRouletteFields)
  const actualFieldBet: ValueField | GroupField = fields[selectedField.type][selectedField.index]
  const diff: number = actualFieldBet.chips - action.payload.value

  yield put(createPlaceChipsAction(action.payload.value))
  yield put(createIncreaseBetChipsAction(diff))
}

const getChips = (winNumber: BasicField, fields: Fields) => {
  const scopes: string[] = Object.keys(fields)
  let wonChips = 0
  let lostChips = 0

  scopes.forEach((scope: string) => {
    fields[scope as keyof Fields].forEach((field: ValueField | GroupField) => {
      if (field.chips === 0) return

      if (field.value === winNumber.value) {
        wonChips += field.multiple * field.chips
        return
      }

      if (Array.isArray(field.value) && field.value.includes(Number(winNumber.value))) {
        wonChips += field.multiple * field.chips
        return
      }

      if (field.value === winNumber.color) {
        wonChips += field.multiple * field.chips
        return
      }

      lostChips += field.chips
    })
  })

  return { wonChips, lostChips }
}

export const rouletteStartSaga = function* () {
  const fields: Fields = yield select(getRouletteFields)
  const numbers: ValueField[] = fields.numbers.concat(fields.zeros)
  const rouletteNumber = generateRandomRouletteResult(numbers)

  const chips = getChips(rouletteNumber, fields)

  yield put(
    createAddHistoryMessageAction({
      msg: 'winningNumberIs',
      winNumber: rouletteNumber.value,
      chips: chips.wonChips,
    }),
  )

  yield put(createLastWonNumberAction(rouletteNumber.value))
  yield put(createIncreaseBetChipsAction(null))
  yield put(createIncreaseLostChipsAction(chips.lostChips))
  yield put(createIncreaseWonChipsAction(chips.wonChips))
  yield put(createIncreaseGameAction())
  yield put(createCleanDeskAction())

  const actualChips: number = yield select(getUserChips)

  if (actualChips <= 0) yield put(createLostGameAction())
}
