import { RootState, FieldType } from '../types'

export const getRouletteFields = (state: RootState) => {
  return state.roulette.fields
}

export const getSelectedField = (state: RootState) => {
  return state.roulette.selectedField
}

export const getRouletteVariant = (state: RootState) => {
  return state.roulette.variant
}

export const getLastWonNumber = (state: RootState) => {
  return state.roulette.lastWonNumber
}

export const getBetFieldById = (state: RootState, type: FieldType, index: number) => state.roulette.fields[type][index]
