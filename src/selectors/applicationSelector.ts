import { RootState } from '../types'

export const getApplicationStatus = (state: RootState) => {
  return state.application.game.status
}

export const getUserName = (state: RootState) => {
  return state.application.record.playerName
}

export const getUserChips = (state: RootState) => {
  const { initialChips, wonChips, lostChips } = state.application.record
  return initialChips + wonChips - lostChips + state.application.game.actualBet
}

export const getGameHistory = (state: RootState) => {
  return state.application.game.history
}

export const getGameRecord = (state: RootState) => {
  return state.application.record
}

export const getResultList = (state: RootState) => {
  return state.application.get.response
}

export const getResultsState = (state: RootState) => {
  return state.application.get.state
}

export const getPostState = (state: RootState) => {
  return state.application.post.state
}

export const getLostGame = (state: RootState) => {
  return state.application.game.lost
}
