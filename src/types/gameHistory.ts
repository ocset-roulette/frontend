export type GameHistory = {
  msg: string
  chips: number
  winNumber?: number
}
