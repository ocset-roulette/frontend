export type RecordState = {
  playerName: string
  initialChips: number
  playedGames: number
  biggestWin: number
  wonChips: number
  lostChips: number
}
