import { GameStatus } from './game'
import { GameHistory } from './gameHistory'
import { RecordState } from './record'
import { GetRequestState, PostRequestState } from './request'
import { FieldLocation, Fields, RouletteVariant } from './roulette'

export type RouletteState = {
  fields: Fields
  variant: RouletteVariant
  selectedField: null | FieldLocation
  lastWonNumber: null | number
}

export type GameState = {
  status: GameStatus
  history: GameHistory[]
  actualBet: number
  lost: boolean
}

export type ApplicationState = {
  game: GameState
  record: RecordState
  get: GetRequestState
  post: PostRequestState
}

export type RootState = {
  roulette: RouletteState
  application: ApplicationState
}
