export type Request = {
  method: 'GET' | 'POST'
  headers?: {
    Accept: string
    'Content-Type': string
  }
  body?: string
}
