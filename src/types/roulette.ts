export enum Color {
  Red = 'RED',
  Black = 'BLACK',
  Green = 'GREEN',
}

export enum FieldType {
  numbers = 'numbers',
  columns = 'columns',
  evenOdds = 'evenOdds',
  dozens = 'dozens',
  smallBig = 'smallBig',
  colors = 'colors',
  zeros = 'zeros',
}

export enum RouletteVariant {
  European = 'EUROPEAN',
  American = 'AMERICAN',
}

export type Field = {
  number: string
  color: Color
}

export type BasicField = {
  checked: boolean
  chips: number
  multiple: number
  color?: Color
  value?: string | number | Color | number[]
  label?: string
}

export type ValueField = BasicField & {
  color: Color
  value: string | number | Color
}

export type GroupField = BasicField & {
  value: number[]
  label: string
}

export type Fields = {
  numbers: ValueField[]
  columns: GroupField[]
  evenOdds: GroupField[]
  dozens: GroupField[]
  smallBig: GroupField[]
  colors: ValueField[]
  zeros: ValueField[]
}

export type FieldLocation = {
  type: FieldType
  index: number
}
