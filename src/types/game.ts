export enum GameStatus {
  initial = 'INITIAL',
  running = 'RUNNING',
  end = 'END',
  results = 'RESULTS',
}

export type Game = {
  state: GameStatus
}
