import { RecordState } from './record'

export enum Status {
  init = 'init',
  requesting = 'requesting',
  error = 'error',
  success = 'success',
}

export type GetRequestState = {
  response: RecordState[]
  state: Status
  error: string | null
}

export type PostRequestState = {
  response?: RecordState
  state: Status
  error: string | null
}
