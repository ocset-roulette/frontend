import React, { useState, ChangeEvent, MouseEvent } from 'react'
import { useTranslation } from 'react-i18next'
import { useDispatch } from 'react-redux'

import { createPlayGameAction, createShowResultsAction } from '../../actions'
import { RouletteVariant } from '../../types/roulette'
import { Button } from '../base/Button'
import { TextInput } from '../base/TextInput'
import styles from './UserInputs.module.scss'

export const UserInputs = () => {
  const [userName, setUserName] = useState<string>('anonym')
  const [userNameError, setUserNameError] = useState<string>('')
  const [initialChips, setInitialChips] = useState<number>(100)
  const [initialChipsError, setInitialChipsError] = useState<string>('')

  const { t } = useTranslation()
  const dispatch = useDispatch()

  const handleUserNameChange = (e: ChangeEvent<HTMLInputElement>) => {
    setUserName(e.target.value)
  }

  const handleInititalChipsChange = (e: ChangeEvent<HTMLInputElement>) => {
    setInitialChips(Number(e.target.value))
  }

  const handleClick = (variant: RouletteVariant) => {
    if (userName.length > 0 && initialChips > 0 && initialChips <= 100) {
      dispatch(createPlayGameAction(userName, initialChips, variant))
    }

    if (userName.length < 1) {
      setUserNameError(t('userNameError'))
    }

    if (initialChips < 1 || initialChips > 100) {
      setInitialChipsError(t('initialChipsError'))
    }
  }

  const showResultList = (e: MouseEvent<HTMLElement>) => {
    e.preventDefault()
    dispatch(createShowResultsAction())
  }

  return (
    <div className={styles.userInputs}>
      <TextInput
        type='text'
        value={userName}
        onChange={handleUserNameChange}
        label={t('userName')}
        error={userNameError}
      />
      <TextInput
        type='number'
        value={initialChips}
        onChange={handleInititalChipsChange}
        label={t('chipNumber')}
        error={initialChipsError}
      />
      <p>{t('chipsInfo')}</p>
      <Button
        label={t('european')}
        onClick={() => {
          handleClick(RouletteVariant.European)
        }}
      />
      <span>{t('or')}</span>
      <Button
        label={t('american')}
        onClick={() => {
          handleClick(RouletteVariant.American)
        }}
      />
      <div className={styles.resultList}>
        {t('youCanCheck')}
        <a href='#' onClick={showResultList}>
          {t('resultList')}
        </a>
      </div>
    </div>
  )
}
