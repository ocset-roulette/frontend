import React from 'react'

import { Status } from '../../types'
import styles from './RequestState.module.scss'

export type Props = {
  requestingText: string
  errorText: string
  status: Status
}

export const RequstState = ({ requestingText, errorText, status }: Props) => {
  return (
    <div className={styles.requestState}>
      {status === Status.requesting && (
        <div className={styles.requestMessage}>
          <span className={styles.loadingText}>
            <img src='images/loading.gif' alt='' />
            {requestingText}
          </span>
        </div>
      )}
      {status === Status.error && <div className={styles.errorText}>{errorText}</div>}
    </div>
  )
}
