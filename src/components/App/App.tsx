import React from 'react'
import { useSelector } from 'react-redux'

import { GamePanel } from '../GamePanel'
import { Roulette } from '../Roulette'
import { BetBox } from '../BetBox'
import styles from './App.module.scss'
import { UserInfo } from '../UserInfo'
import { getApplicationStatus } from '../../selectors'
import { GameStatus } from '../../types'
import { BetModalWindow } from '../BetModalWindow'

function App() {
  const gameStatus: GameStatus = useSelector(getApplicationStatus)

  return (
    <>
      <div className={styles.topPanel}>
        <UserInfo />
      </div>

      {gameStatus === GameStatus.running && (
        <div className={styles.rouletteContainer}>
          <Roulette />
        </div>
      )}

      {gameStatus === GameStatus.running && (
        <div className={styles.betBoxContainer}>
          <BetBox />
        </div>
      )}

      <div className={styles.rightPanel}>
        <GamePanel />
      </div>

      <BetModalWindow />
    </>
  )
}

export default App
