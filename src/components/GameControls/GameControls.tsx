import React from 'react'
import { useTranslation } from 'react-i18next'
import { useDispatch, useSelector } from 'react-redux'

import { createEndGameAction, createStartRouletteAction } from '../../actions'
import { getGameHistory, getLostGame } from '../../selectors'
import { GameHistory } from '../../types'
import { Button } from '../base/Button'
import styles from './GameControls.module.scss'

export const GameControls = () => {
  const { t } = useTranslation()
  const dispatch = useDispatch()
  const gameHistory: GameHistory[] = useSelector(getGameHistory)
  const isLostGame: boolean = useSelector(getLostGame)

  const handleSpin = () => {
    dispatch(createStartRouletteAction())
  }

  const handleEnd = () => {
    dispatch(createEndGameAction())
  }

  return (
    <>
      {/* history */}
      <div className={styles.gameControlSection}>
        <h2>{t('gameHistory')}</h2>
        <ul>
          {gameHistory.slice(Math.max(gameHistory.length - 5, 0)).map((item, index) => (
            <li key={index}>
              {t(item.msg)} {item.winNumber} ({t('youGet')} {item.chips} {t('chips')})
            </li>
          ))}
        </ul>
      </div>
      {/* controls */}
      <div className={styles.gameControlSection}>
        {!isLostGame && (
          <>
            <span>{t('placeYourBets')}</span>
            <Button label={t('startRoulette')} onClick={handleSpin} />
            <span>{t('areYouDone')}</span>
          </>
        )}
        {isLostGame && <span>{t('youLost')}</span>}
        <Button label={t('endGame')} onClick={handleEnd} />
      </div>
    </>
  )
}
