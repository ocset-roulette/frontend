import React, { useCallback, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { createBetAction, createCancelBetAction } from '../../actions'
import { getRouletteFields, getSelectedField } from '../../selectors'
import { FieldLocation, Fields } from '../../types/roulette'
import { BetInput } from '../BetInput'
import styles from './BetModalWindow.module.scss'

export const BetModalWindow = () => {
  const dispatch = useDispatch()

  const selectedField: FieldLocation | null = useSelector(getSelectedField)
  const fields: Fields = useSelector(getRouletteFields)

  const handleUserKeyPress = useCallback(event => {
    if (event.key === 'Escape') {
      dispatch(createCancelBetAction())
    }
  }, [])

  useEffect(() => {
    window.addEventListener('keydown', handleUserKeyPress)
    return () => {
      window.removeEventListener('keydown', handleUserKeyPress)
    }
  }, [handleUserKeyPress])

  const closeWindow = () => {
    dispatch(createCancelBetAction())
  }

  const bet = (value: number) => {
    dispatch(createBetAction(value))
  }

  return (
    <>
      {selectedField && (
        <div className={styles.betModalWindow} onMouseDown={closeWindow}>
          <div
            onMouseDown={e => {
              e.stopPropagation()
            }}
          >
            <BetInput
              onCancel={closeWindow}
              onBet={bet}
              value={fields[selectedField.type][selectedField.index].chips}
            />
          </div>
        </div>
      )}
    </>
  )
}
