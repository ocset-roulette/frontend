import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'

import { createLastWonNumberAction, createSelectFieldAction } from '../../actions'
import { getBetFieldById } from '../../selectors'
import { Color, FieldType, GroupField, ValueField } from '../../types/roulette'
import classnames from '../../utils/classnames'
import styles from './BetField.module.scss'
import { RootState } from '../../types'

export type Props = {
  chips: number
  value: number | string
  color?: Color | undefined
  fullSize?: boolean
  type: FieldType
  index: number
  hiddenText?: boolean
}

export const BetField = ({ fullSize = false, type, index, hiddenText }: Props) => {
  const dispatch = useDispatch()
  const { t } = useTranslation()

  const field: ValueField | GroupField = useSelector((state: RootState) => getBetFieldById(state, type, index))
  const { chips, color, value, label, checked } = field

  const selectField = () => {
    dispatch(createSelectFieldAction(type, index))
    dispatch(createLastWonNumberAction(null))
  }

  return (
    <div
      className={classnames(styles.field, {
        [styles.red]: color === Color.Red,
        [styles.black]: color === Color.Black,
        [styles.green]: color === Color.Green,
        [styles.checked]: checked,
        [styles.fullSize]: fullSize,
      })}
      onClick={selectField}
    >
      {!hiddenText ? (label ? t(label) : value) : ''}
      {chips > 0 && <span className={styles.chips}>({chips})</span>}
    </div>
  )
}
