import React from 'react'
import { useTranslation } from 'react-i18next'
import { useSelector } from 'react-redux'

import { getResultList, getResultsState } from '../../selectors'
import { Status, RecordState } from '../../types'
import { Button } from '../base/Button'
import { Record } from '../base/Record'
import { RequstState } from '../RequestState'
import styles from './ResultList.module.scss'

export const ResultList = () => {
  const { t } = useTranslation()
  const resultsState: Status = useSelector(getResultsState)
  const results: RecordState[] = useSelector(getResultList)

  const backToStart = () => {
    location.reload()
  }

  return (
    <div className={styles.resultList}>
      <h2>{t('resultList')}</h2>

      <RequstState errorText={t('getError')} requestingText={t('loading')} status={resultsState} />

      <ul className={styles.results}>
        {results.map((result: RecordState, index: number) => (
          <li key={index}>
            <Record gameRecord={result} />
          </li>
        ))}
      </ul>

      <Button label={t('back')} onClick={backToStart} />
    </div>
  )
}
