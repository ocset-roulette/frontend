import React, { useState, ChangeEvent } from 'react'
import { useTranslation } from 'react-i18next'
import { useSelector } from 'react-redux'

import { getUserChips } from '../../selectors'
import { Button } from '../base/Button'
import { TextInput } from '../base/TextInput'
import styles from './BetInput.module.scss'

export type Props = {
  onCancel: () => void
  onBet: (e: number) => void
  value: number
}

export const BetInput = ({ onCancel, onBet, value }: Props) => {
  const { t } = useTranslation()
  const [chips, setChips] = useState<number>(value)
  const [error, setError] = useState<string>('')
  const userChips: number = useSelector(getUserChips)

  const handleBet = () => {
    if (chips > value && userChips < chips - value) {
      setError(t('betError'))
      return
    }
    onBet(chips)
  }

  const handleCancel = () => {
    onCancel()
  }

  const handleChipsChange = (e: ChangeEvent<HTMLInputElement>) => {
    setChips(Number(e.target.value))
  }

  return (
    <div className={styles.betInput}>
      <TextInput type='number' value={chips} onChange={handleChipsChange} error={error} />
      <div className={styles.buttonContainer}>
        <Button label={t('bet')} onClick={handleBet} />
        <Button label={t('cancel')} onClick={handleCancel} />
      </div>
    </div>
  )
}
