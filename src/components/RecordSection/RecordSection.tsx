import React from 'react'
import { useTranslation } from 'react-i18next'
import { useDispatch, useSelector } from 'react-redux'

import { createSendRecordAction } from '../../actions'
import { getGameRecord, getPostState } from '../../selectors'
import { RecordState, Status } from '../../types'
import { Button } from '../base/Button'
import { Record } from '../base/Record'
import { RequstState } from '../RequestState'
import styles from './RecordSection.module.scss'

export const RecordSection = () => {
  const gameRecord: RecordState = useSelector(getGameRecord)
  const postState: Status = useSelector(getPostState)
  const dispatch = useDispatch()
  const { t } = useTranslation()

  const handleRecordSend = () => {
    dispatch(createSendRecordAction(gameRecord))
  }

  const newGame = () => {
    location.reload()
  }

  return (
    <div className={styles.recordSection}>
      <h2>{t('thanksForGame')}</h2>
      <h3>{t('hereAreResults')}</h3>

      <Record gameRecord={gameRecord} />
      <p>{t('chipsInfo')}</p>

      <Button label={t('sendRecord')} onClick={handleRecordSend} />
      <span>{t('or')}</span>
      <Button label={t('newGame')} onClick={newGame} />

      <RequstState errorText={t('postError')} requestingText={t('sending')} status={postState} />
    </div>
  )
}
