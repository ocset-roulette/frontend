import React from 'react'
import { useSelector } from 'react-redux'

import { getApplicationStatus } from '../../selectors'
import { GameStatus } from '../../types'
import { GameControls } from '../GameControls'
import { RecordSection } from '../RecordSection'
import { ResultList } from '../ResultList'
import { UserInputs } from '../UserInputs'
import styles from './GamePanel.module.scss'

export const GamePanel = () => {
  const gameStatus: GameStatus = useSelector(getApplicationStatus)

  return (
    <div className={styles.gamePanel}>
      {gameStatus === GameStatus.initial && <UserInputs />}
      {gameStatus === GameStatus.running && <GameControls />}
      {gameStatus === GameStatus.end && <RecordSection />}
      {gameStatus === GameStatus.results && <ResultList />}
    </div>
  )
}
