import React from 'react'
import { useSelector } from 'react-redux'

import { Field, RouletteVariant } from '../../types/roulette'
import styles from './Roulette.module.scss'
import { RouletteField } from '../RouletteField'
import { getLastWonNumber, getRouletteVariant } from '../../selectors'
import { american, european } from '../../roulette'

export const Roulette = () => {
  const variant: RouletteVariant = useSelector(getRouletteVariant)
  const lastWonNumber: number | null = useSelector(getLastWonNumber)
  const fields: Field[] = variant === RouletteVariant.European ? european : american

  return (
    <div className={styles.roulette}>
      {lastWonNumber && <div className={styles.lastWonNumber}>{lastWonNumber}</div>}
      {fields.map(field => (
        <RouletteField color={field.color} number={field.number} key={`roulette-${field.number}`} />
      ))}
    </div>
  )
}
