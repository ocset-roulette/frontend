import React from 'react'
import { useSelector } from 'react-redux'

import { getRouletteFields } from '../../selectors'
import { Fields, FieldType } from '../../types/roulette'
import { BetField } from '../BetField'
import styles from './BetBox.module.scss'

export const BetBox = () => {
  const fields: Fields = useSelector(getRouletteFields)

  return (
    <div className={styles.betContainer}>
      <div className={styles.betBox}>
        {/* Left */}
        <div className={styles.leftBetBox}>
          {fields.zeros.map((field, index) => (
            <BetField
              key={`betBoxField-${field.value}`}
              chips={field.chips}
              value={field.value}
              color={field.color}
              type={FieldType.zeros}
              index={index}
            />
          ))}
        </div>
        {/* Middle */}
        <div className={styles.middleBetBox}>
          {fields.numbers.map((field, index) => (
            <BetField
              key={`betBoxField-${field.value}`}
              chips={field.chips}
              value={field.value}
              color={field.color}
              type={FieldType.numbers}
              index={index}
            />
          ))}
        </div>
        {/* Right */}
        <div className={styles.rightBetBox}>
          {fields.columns.map((field, index) => (
            <BetField
              key={`betBoxColumn-${index}`}
              chips={field.chips}
              value={field.label}
              type={FieldType.columns}
              index={index}
            />
          ))}
        </div>
      </div>
      {/* Bottom */}
      <div className={styles.bottomBetBox}>
        {/* Bottom - top */}
        <div className={styles.bottomTopBetBox}>
          {fields.dozens.map((field, index) => (
            <BetField
              key={`betBoxDozen-${index}`}
              chips={field.chips}
              value={field.label}
              fullSize={true}
              type={FieldType.dozens}
              index={index}
            />
          ))}
        </div>
        {/* Bottom - bottom */}
        <div className={styles.bottomBottomBetBox}>
          <BetField
            chips={fields.smallBig[0].chips}
            value={fields.smallBig[0].label}
            fullSize={true}
            type={FieldType.smallBig}
            index={0}
          />

          <BetField
            chips={fields.evenOdds[1].chips}
            value={fields.evenOdds[1].label}
            fullSize={true}
            type={FieldType.evenOdds}
            index={1}
          />

          <BetField
            chips={fields.colors[0].chips}
            value={''}
            color={fields.colors[0].color}
            fullSize={true}
            type={FieldType.colors}
            index={0}
            hiddenText={true}
          />

          <BetField
            chips={fields.colors[1].chips}
            value={''}
            color={fields.colors[1].color}
            fullSize={true}
            type={FieldType.colors}
            index={1}
            hiddenText={true}
          />

          <BetField
            chips={fields.evenOdds[0].chips}
            value={fields.evenOdds[0].label}
            fullSize={true}
            type={FieldType.evenOdds}
            index={0}
          />

          <BetField
            chips={fields.smallBig[1].chips}
            value={fields.smallBig[1].label}
            fullSize={true}
            type={FieldType.smallBig}
            index={1}
          />
        </div>
      </div>
    </div>
  )
}
