import React from 'react'
import { useSelector } from 'react-redux'

import { getLastWonNumber } from '../../selectors'
import { Color, Field } from '../../types/roulette'
import classnames from '../../utils/classnames'
import styles from './RouletteField.module.scss'

export const RouletteField = ({ number, color }: Field) => {
  const lastWonNumber: number | null = useSelector(getLastWonNumber)

  return (
    <div
      className={classnames(styles.rouletteField, {
        [styles.red]: color === Color.Red,
        [styles.black]: color === Color.Black,
        [styles.green]: color === Color.Green,
        [styles.selected]: lastWonNumber?.toString() === number.toString(),
      })}
    >
      {number}
    </div>
  )
}
