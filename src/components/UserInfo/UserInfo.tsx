import React from 'react'
import { useTranslation } from 'react-i18next'
import { useSelector } from 'react-redux'

import { getApplicationStatus, getUserName, getUserChips } from '../../selectors'
import { GameStatus } from '../../types'
import styles from './UserInfo.module.scss'

export const UserInfo = () => {
  const gameStatus: GameStatus = useSelector(getApplicationStatus)
  const userName: string = useSelector(getUserName)
  const userChips: number = useSelector(getUserChips)
  const { t } = useTranslation()

  return (
    <>
      {gameStatus === GameStatus.running && (
        <div className={styles.userInfo}>{`${userName} (${userChips} ${t('chips')})`}</div>
      )}
    </>
  )
}
