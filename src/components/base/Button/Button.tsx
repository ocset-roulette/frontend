import React from 'react'
import styles from './Button.module.scss'

export type Props = {
  label: string
  onClick?: () => void
}

export const Button = ({ label, onClick }: Props) => {
  return (
    <button className={styles.button} onClick={onClick}>
      {label}
    </button>
  )
}
