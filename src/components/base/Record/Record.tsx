import React from 'react'
import { useTranslation } from 'react-i18next'

import { RecordState } from '../../../types/record'
import styles from './Record.module.scss'

export type Props = {
  gameRecord: RecordState
}

export const Record = ({ gameRecord }: Props) => {
  const { t } = useTranslation()

  return (
    <ul className={styles.record}>
      <li>
        {t('userName')}: {gameRecord.playerName}
      </li>
      <li>
        {t('initialChips')}: {gameRecord.initialChips}
      </li>
      <li>
        {t('playedGames')}: {gameRecord.playedGames}
      </li>
      <li>
        {t('biggestWin')}: {gameRecord.biggestWin}
      </li>
      <li>
        {t('wonChips')}: {gameRecord.wonChips}
      </li>
      <li>
        {t('lostChips')}: {gameRecord.lostChips}
      </li>
      <li>
        {t('finalChipState')}: {gameRecord.initialChips + gameRecord.wonChips - gameRecord.lostChips}
      </li>
    </ul>
  )
}
