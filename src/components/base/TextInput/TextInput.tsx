import React, { ChangeEvent } from 'react'

import classnames from '../../../utils/classnames'
import styles from './TextInput.module.scss'

export type Props = {
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void
  value: string | number
  type: 'number' | 'text'
  placeHolder?: string
  label?: string
  error?: string
}

export const TextInput = ({ onChange, value, type, placeHolder = '', label = '', error = '' }: Props) => {
  return (
    <>
      <label className={styles.label}>{label}</label>
      <input
        type={type}
        className={classnames(styles.textInput, {
          [styles.textInputError]: error,
        })}
        onChange={onChange}
        value={value}
        placeholder={placeHolder}
      />
      <div className={styles.errorText}>{error}</div>
    </>
  )
}
