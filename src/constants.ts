export const START_GAME = 'START_GAME'
export const SET_USER_NAME = 'SET_USER_NAME'
export const SET_INITIAL_CHIPS = 'SET_INITIAL_CHIPS'
export const SET_GAME_STATUS = 'SET_GAME_STATUS'
export const ADD_HISTORY_MESSAGE = 'ADD_HISTORY_MESSAGE'
export const INCREASE_BET_CHIPS = 'INCREASE_CHIPS'
export const INCREASE_LOST_CHIPS = 'INCREASE_LOST_CHIPS'
export const INCREASE_WON_CHIPS = 'INCREASE_WON_CHIPS'
export const END_GAME = 'END_GAME'
export const SEND_RECORD = 'SEND_RECORD'
export const SHOW_RESULTS = 'SHOW_RESULTS'
export const GET_RESULTS = 'GET_RESULTS'
export const INCREASE_GAME_NUMBER = 'INCREASE_GAME_NUMBER'

export const SET_SELECTED_FIELD = 'SET_SELECTED_FIELD'
export const SET_BET = 'SET_BET'
export const SET_PLACE_CHIPS = 'SET_PLACE_CHIPS'
export const SET_ROULETTE_START = 'SET_ROULETTE_START'
export const CLEAN_DESK = 'CLEAN_DESK'
export const CANCEL_BET = 'CANCEL_BET'
export const SET_ROULETTE_VARIANT = 'SET_ROULETTE_VARIANT'
export const SET_LAST_WON_NUMBER = 'SET_LAST_WON_NUMBER'

export const GET_RESULTS_REQUEST = 'GET_RESULTS_REQUEST'
export const GET_RESULTS_SUCCESS = 'GET_RESULTS_SUCCESS'
export const GET_RESULTS_ERROR = 'GET_RESULTS_ERROR'

export const POST_RECORD_REQUEST = 'POST_RECORD_REQUEST'
export const POST_RECORD_SUCCESS = 'POST_RECORD_SUCCESS'
export const POST_RECORD_ERROR = 'POST_RECORD_ERROR'

export const CALL_API = 'CALL_API'
export const LOST_GAME = 'LOST_GAME'
