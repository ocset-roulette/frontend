import produce from 'immer'
import { combineReducers } from 'redux'
import {
  SET_GAME_STATUS,
  SET_INITIAL_CHIPS,
  SET_USER_NAME,
  ADD_HISTORY_MESSAGE,
  INCREASE_BET_CHIPS,
  INCREASE_LOST_CHIPS,
  INCREASE_WON_CHIPS,
  INCREASE_GAME_NUMBER,
  GET_RESULTS_REQUEST,
  GET_RESULTS_SUCCESS,
  GET_RESULTS_ERROR,
  POST_RECORD_REQUEST,
  POST_RECORD_SUCCESS,
  POST_RECORD_ERROR,
  LOST_GAME,
} from '../constants'
import { GameState, GameStatus, GetRequestState, PostRequestState, Status, RecordState } from '../types'

const createRequestReducer = (
  requestType: string,
  successType: string,
  errorType: string,
  initState: GetRequestState | PostRequestState,
) => {
  return produce((draftState, action) => {
    switch (action.type) {
      case requestType:
        draftState.state = Status.requesting
        draftState.error = ''
        draftState.response = []
        break
      case successType:
        draftState.state = Status.success
        draftState.response = action.payload.response
        break
      case errorType:
        draftState.state = Status.error
        draftState.error = action.payload.error
    }
  }, initState)
}

const initGetRequestState: GetRequestState = {
  response: [],
  error: '',
  state: Status.init,
}

const initPostRequestState: PostRequestState = {
  error: '',
  state: Status.init,
}

const postReducer = createRequestReducer(
  POST_RECORD_REQUEST,
  POST_RECORD_SUCCESS,
  POST_RECORD_ERROR,
  initPostRequestState,
)
const getReducer = createRequestReducer(
  GET_RESULTS_REQUEST,
  GET_RESULTS_SUCCESS,
  GET_RESULTS_ERROR,
  initGetRequestState,
)

const initRecord: RecordState = {
  playerName: 'anonym',
  initialChips: 100,
  playedGames: 0,
  biggestWin: 0,
  wonChips: 0,
  lostChips: 0,
}

const recordReducer = produce((draftState, action) => {
  switch (action.type) {
    case SET_USER_NAME: {
      draftState.playerName = action.payload.userName
      break
    }
    case SET_INITIAL_CHIPS: {
      draftState.initialChips = action.payload.initialChips
      break
    }
    case INCREASE_LOST_CHIPS: {
      draftState.lostChips += action.payload.number
      break
    }
    case INCREASE_WON_CHIPS: {
      draftState.wonChips += action.payload.number

      if (draftState.biggestWin < action.payload.number) {
        draftState.biggestWin = action.payload.number
      }

      break
    }
    case INCREASE_GAME_NUMBER: {
      draftState.playedGames++
      break
    }
  }
}, initRecord)

export const initGameState: GameState = {
  status: GameStatus.initial,
  history: [],
  actualBet: 0,
  lost: false,
}

const gameReducer = produce((draftState, action) => {
  switch (action.type) {
    case SET_GAME_STATUS: {
      draftState.status = action.payload.status
      break
    }
    case ADD_HISTORY_MESSAGE: {
      draftState.history.push(action.payload.message)
      break
    }
    case INCREASE_BET_CHIPS: {
      if (action.payload.number === null) draftState.actualBet = 0
      else draftState.actualBet += action.payload.number
      break
    }
    case LOST_GAME: {
      draftState.lost = true
      break
    }
  }
}, initGameState)

export const applicationReducer = combineReducers({
  game: gameReducer,
  record: recordReducer,
  post: postReducer,
  get: getReducer,
})
