import { combineReducers } from 'redux'
import { applicationReducer } from './applicationReducer'
import { rouletteReducer } from './rouletteReducer'

export const rootReducer = combineReducers({
  application: applicationReducer,
  roulette: rouletteReducer,
})
