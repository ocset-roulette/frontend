import produce from 'immer'

import {
  CANCEL_BET,
  CLEAN_DESK,
  SET_PLACE_CHIPS,
  SET_SELECTED_FIELD,
  SET_ROULETTE_VARIANT,
  SET_LAST_WON_NUMBER,
} from '../constants'
import { european, american } from '../roulette'
import { RouletteVariant } from '../types/roulette'
import { RouletteState } from '../types/state'
import { prepareFields } from '../utils/roulette'

export const initRouletteState: RouletteState = {
  fields: prepareFields(european),
  variant: RouletteVariant.European,
  selectedField: null,
  lastWonNumber: null,
}

export const rouletteReducer = produce((draftState, action) => {
  switch (action.type) {
    case SET_SELECTED_FIELD: {
      if (draftState.selectedField) {
        draftState.fields[draftState.selectedField.type][draftState.selectedField.index].checked = false
      }

      draftState.selectedField = {
        type: action.payload.type,
        index: action.payload.index,
      }

      draftState.fields[draftState.selectedField.type][draftState.selectedField.index].checked = true
      break
    }
    case SET_PLACE_CHIPS: {
      if (draftState.selectedField) {
        draftState.fields[draftState.selectedField.type][draftState.selectedField.index].chips = action.payload.value
        draftState.fields[draftState.selectedField.type][draftState.selectedField.index].checked = false
      }
      draftState.selectedField = null
      break
    }
    case CLEAN_DESK: {
      draftState.selectedField = null
      if (draftState.variant === RouletteVariant.American) {
        draftState.fields = prepareFields(american)
      } else {
        draftState.fields = prepareFields(european)
      }
      break
    }
    case CANCEL_BET: {
      if (draftState.selectedField) {
        draftState.fields[draftState.selectedField.type][draftState.selectedField.index].checked = false
      }
      draftState.selectedField = null
      break
    }
    case SET_ROULETTE_VARIANT: {
      if (RouletteVariant.American === action.payload.variant) {
        draftState.fields = prepareFields(american)
        draftState.variant = RouletteVariant.American
      } else {
        draftState.fields = prepareFields(european)
        draftState.variant = RouletteVariant.European
      }
      break
    }
    case SET_LAST_WON_NUMBER: {
      draftState.lastWonNumber = action.payload.value
    }
  }
}, initRouletteState)
