import {
  SET_BET,
  SET_SELECTED_FIELD,
  SET_PLACE_CHIPS,
  SET_ROULETTE_START,
  CLEAN_DESK,
  CANCEL_BET,
  END_GAME,
  SET_ROULETTE_VARIANT,
  SET_LAST_WON_NUMBER,
} from '../constants'

import { FieldType, RouletteVariant } from '../types/roulette'

export const createSelectFieldAction = (type: FieldType, index: number) => ({
  type: SET_SELECTED_FIELD,
  payload: { type, index },
})

export const createBetAction = (value: number) => ({
  type: SET_BET,
  payload: { value },
})

export const createCancelBetAction = () => ({
  type: CANCEL_BET,
})

export const createPlaceChipsAction = (value: number) => ({
  type: SET_PLACE_CHIPS,
  payload: { value },
})

export const createStartRouletteAction = () => ({
  type: SET_ROULETTE_START,
})

export const createCleanDeskAction = () => ({
  type: CLEAN_DESK,
})

export const createEndGameAction = () => ({
  type: END_GAME,
})

export const createSelectRouletteVariantAction = (variant: RouletteVariant) => ({
  type: SET_ROULETTE_VARIANT,
  payload: { variant },
})

export const createLastWonNumberAction = (value: number | null) => ({
  type: SET_LAST_WON_NUMBER,
  payload: { value },
})
