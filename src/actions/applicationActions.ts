import {
  START_GAME,
  SET_USER_NAME,
  SET_INITIAL_CHIPS,
  SET_GAME_STATUS,
  ADD_HISTORY_MESSAGE,
  INCREASE_BET_CHIPS,
  INCREASE_LOST_CHIPS,
  INCREASE_WON_CHIPS,
  SEND_RECORD,
  SHOW_RESULTS,
  GET_RESULTS,
  INCREASE_GAME_NUMBER,
  GET_RESULTS_REQUEST,
  GET_RESULTS_ERROR,
  GET_RESULTS_SUCCESS,
  POST_RECORD_REQUEST,
  POST_RECORD_SUCCESS,
  POST_RECORD_ERROR,
  LOST_GAME,
} from '../constants'
import { GameHistory, GameStatus, RecordState } from '../types'
import { RouletteVariant } from '../types/roulette'

export const createPlayGameAction = (userName: string, initialChips: number, rouletteVariant: RouletteVariant) => ({
  type: START_GAME,
  payload: { userName, initialChips, rouletteVariant },
})

export const createSaveUserNameAction = (userName: string) => ({
  type: SET_USER_NAME,
  payload: { userName },
})

export const createSaveInitialChipsAction = (initialChips: number) => ({
  type: SET_INITIAL_CHIPS,
  payload: { initialChips },
})

export const createSetGameStatusAction = (status: GameStatus) => ({
  type: SET_GAME_STATUS,
  payload: { status },
})

export const createAddHistoryMessageAction = (message: GameHistory) => ({
  type: ADD_HISTORY_MESSAGE,
  payload: { message },
})

export const createIncreaseBetChipsAction = (number: number | null) => ({
  type: INCREASE_BET_CHIPS,
  payload: { number },
})

export const createIncreaseLostChipsAction = (number: number) => ({
  type: INCREASE_LOST_CHIPS,
  payload: { number },
})

export const createIncreaseWonChipsAction = (number: number) => ({
  type: INCREASE_WON_CHIPS,
  payload: { number },
})

export const createShowResultsAction = () => ({
  type: SHOW_RESULTS,
})

export const createIncreaseGameAction = () => ({
  type: INCREASE_GAME_NUMBER,
})

export const createGetResultsAction = () => ({
  type: GET_RESULTS,
  payload: {
    request: { method: 'GET' },
    types: [GET_RESULTS_REQUEST, GET_RESULTS_SUCCESS, GET_RESULTS_ERROR],
  },
})

export const createSendRecordAction = (record: RecordState) => ({
  type: SEND_RECORD,
  payload: {
    request: { method: 'POST', body: JSON.stringify(record) },
    types: [POST_RECORD_REQUEST, POST_RECORD_SUCCESS, POST_RECORD_ERROR],
  },
})

export const createLostGameAction = () => ({
  type: LOST_GAME,
})
