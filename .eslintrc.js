module.exports = {
  "extends": [
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
  ],
  "env": {
      "browser": true,
      "es6": true,
      "node": true
  },
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
      "project": "tsconfig.json",
      "sourceType": "module"
  },
  "plugins": [
      "@typescript-eslint",
      "prettier"
  ],
  "settings": {
      "react": {
          "version": "detect"
      }
  },
  "rules": {
      "prettier/prettier": "warn",
      "react/react-in-jsx-scope": "off",
      "@typescript-eslint/naming-convention": [
        "error",
        {
            "selector": "variable",
            "format": ["PascalCase", "camelCase", "UPPER_CASE"],
            "leadingUnderscore": "allow"
        }
      ],
      "@typescript-eslint/array-type": [
          "error",
          { "default": "array", "readonly": "generic" }
      ],
      "@typescript-eslint/member-delimiter-style": [
          "error",
          {
              "multiline": {
                  "delimiter": "none",
                  "requireLast": true
              },
              "singleline": {
                  "delimiter": "semi",
                  "requireLast": false
              }
          }
      ],
      "@typescript-eslint/no-floating-promises": "error",
      "@typescript-eslint/no-unused-vars": "off",
      "@typescript-eslint/prefer-namespace-keyword": "error",
      "@typescript-eslint/quotes": ["warn", "single", { "avoidEscape": true }],
      "@typescript-eslint/semi": ["warn", "never"],
      "@typescript-eslint/type-annotation-spacing": "error",
      "@typescript-eslint/explicit-function-return-type": "off",
      "@typescript-eslint/explicit-module-boundary-types": "off",
      "@typescript-eslint/no-explicit-any": "off",
      "@typescript-eslint/no-floating-promises": "off",
      "brace-style": [
          "error",
          "1tbs"
      ],
      "eqeqeq": ["error", "smart"],
      "id-blacklist": [
          "off",
          "any",
          "Number",
          "number",
          "String",
          "string",
          "Boolean",
          "boolean",
          "Undefined",
          "undefined"
      ],
      "id-match": "off",
      "no-console": "warn",
      "no-null/no-null": "off",
      "no-trailing-spaces": "warn",
      "no-underscore-dangle": "off",
      "no-var": "error",
      "prefer-const": "error",
      "prefer-object-spread": "error",
      "react/jsx-boolean-value": "off",
      "react/jsx-key": "error",
      "spaced-comment": [
          "error",
          "always",
          {
              "markers": [
                  "/"
              ]
          }
      ]
  }
};
